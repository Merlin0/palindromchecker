from unittest import TestCase
from PalindromeChecker import PalindromeChecker


class testPalindromeChecker(TestCase):
    def test_PalindromChecker_phrase_equals_True(self):
        s = "kajak"

        result = PalindromeChecker(s)

        self.assertEqual(result, True)

    def test_PalindromeChecker_phrase_equals_False(self):
        s = "test"

        result = PalindromeChecker(s)

        self.assertEqual(result, False)

    def test_PalindromeChecker_phrase_with_spaces_equals_True(self):
        s = "nurses run"
        
        result = PalindromeChecker(s)
        
        self.assertEqual(result, True)
        
    def test_PalindromeChecker_phrase_with_spaces_equals_False(self):
        s = "test phrase"
        
        result = PalindromeChecker(s)
        
        self.assertEqual(result, False)

    def test_PalindromeChecker_empty_phrase_equals_True(self):
        result = PalindromeChecker("")

        self.assertEqual(result, True)
