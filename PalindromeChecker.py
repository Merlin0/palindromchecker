def PalindromeChecker(s):
    s = s.replace(" ", "")
    s_reversed = s[::-1]
    if s == s_reversed:
        return True
    return False